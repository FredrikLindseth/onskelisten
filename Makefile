all: double_build clean make_png make_jpg

double_build:
	lualatex FredrikSinØnskeliste.tex
	lualatex FredrikSinØnskeliste.tex

build_english:
	cd english && lualatex wishlist.tex

make_png:
	convert -density 150 FredrikSinØnskeliste.pdf -quality 90 FredrikSinØnskeliste.png

make_jpg:
	convert -density 150 FredrikSinØnskeliste.pdf -quality 90 FredrikSinØnskeliste.jpg

clean:
	@#@ to ignore the echo of the command
	@#-f to will FORCE and not output any error
	@rm -f *.aux # LaTeX auxiliary file
	@rm -f *.toc # auxiliary file for the Table of Contents
	@rm -f *.lof # auxiliary file for the List of Figures
	@rm -f *.lot # auxiliary file for the List of Tables
	@rm -f *.idx # makeidx / index
	@rm -f *.ilg # makeidx / index
	@rm -f *.ind # makeidx / index
	@rm -f *.log # .log: Log file for the compilation
	@rm -f *.out # information about the sectional units that will be used to write the outlines.
	@rm -f *.fls # Core latex/pdflatex auxiliary file
	@rm -f *.blg # Bibliography (BiBTeX) log
	@rm -f main.pdf
	@rm -f *.synctex.gz # latexmake build tool auxillary
	@rm -f *.bbl # Bibliography
	@rm -f *.xdy # Glossary /xindy
	@rm -f *.alg # Glossary /xindy
	@rm -f *.glg # Glossary /xindy
	@rm -f *.acr # Glossary 
	@rm -f *.acn # Glossary 
	@rm -f *.glo # Glossary
	@rm -f *.gls # Glossary
	@rm -f *.glsdefs # Glossary
	@rm -f *.fdb_latexmk # latexmake build tool auxillary
	@rm -f *.brf # hyperref
	@rm -f *.bak # hyperref