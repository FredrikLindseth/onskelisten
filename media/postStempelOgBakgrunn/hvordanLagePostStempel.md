# Hvordan lage poststempel

[Text Along Path - GIMP 2.8 Tutorial For Beginners](https://www.youtube.com/watch?v=XULoT5rTCYE)

- nytt prosjekt 500x500px hvit bakgrunn
- 50% guide vertikal og horisontal
- Ellipse select tools, expand from center, fixed ratio
- Dra til passende og så det er plass til tekst rundt kanten
- Text, hvor som helst "Nordpolen",
- høyreklikk på laget i "layers" og velg "text along path"

...

- nytt lag, bare klikk ok "transparent"
- Sørg for at det er markert
- Klikk på fanen "paths" og klikk "path to selection"
- klikk på edit-fill with Fg color
- roter det transparante laget med teksten for å sentrere.

...

- flip tool, hak av for "vertikal" og for "affect: path"
- tryk på fanen med paths og marker sirkelen, klikk på den for å flippe den (ser ingen - forskjell)
- lag en ny tekst - text to path, nytt lag, path to selection, fill
- roter den nye teksten ned til bunnen

...

- lag en sirkel på utsiden
- edit- stroke path -> stroke line - solid color - line width 6px
