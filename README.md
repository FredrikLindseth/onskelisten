# Ønskelisten til julenissen

Ønskelisten min i latex med bilder som bullets i \itemize og en festlig bakgrunn. Ønskelisten har frimerke og er poststemplet.

I [gamle_ønskelister](gamle_ønskelister) ligger det en engelsk utgave og alle tidligere versjoner.

[FredrikSinØnskeliste.tex](FredrikSinØnskeliste.tex) er den norske og oppdaterte, men alle de endelige for hvert år ligger som releaser/git tags.

[![ønskelisten](./FredrikSinØnskeliste.png)](https://gitlab.com/FredrikLindseth/onskelisten/blob/master/FredrikSin%C3%98nskeliste.pdf)

## Oppdatere ønskkelisten og lage din egen versjon

Det absolutt enkleste er å trykke [her](https://www.overleaf.com/read/pffncngszhfj) og la Overleaf ta hånd om all latexen, man må fortsatt oppdatere bakgrunn og slikt, men det er bare bilderedigering og krever ikke så mye EDBkunnskaper. Bakgrunnene finnes /media/postStempelOgBakgrunn/-mappen

### Krav for å bygge ønskelisten lkoalt

- `Make`
- `latex` / `texlive-full` / `lualatex`
- `babel-norsk`
- `ghostscript`/`imagemagick`

For enkelt å bygge (og faktisk huske det fra år til år) kjør `make` evt `make all`

## Oppdatere poststempel

Man trenger [GIMP](https://www.gimp.org/downloads/) for å redigere stempeltekst

For å oppdatere posttempelet

1. Åpne [poststempelSirkelPostenNorge.xcf](/media/postStempelOgBakgrunn/poststempelSirkelPostenNorge.xcf) i GIMP og velg tekst-verktøyet
2. Klikk på stempelet og endre teksten til det man ønsker. Sørg for at årstallet er rett så ikke juleposten sender brevet i retur pga ugyldig poststempel.
3. Lagre som PDF (CTRL+SHIFT+E export as)

Så må man oppdatere stempelet på frimerket

1. Åpne [stempletFrimerke.xcf](/home/fredrik/workspace/ønskeliste/media/postStempelOgBakgrunn/stempletFrimerke.xcf)
2. Trykk på det runde stempelet og slett stempelet som er der (siden det har feil årstall)

Ved å dra PDFen inn i GIMP så du får `import from PDF`-vinduet

1. Importer PDFen med oppløsning på ca `30 pixels/in` og velg `Image - Color to alpha`, for å bli kvitt det hvite. Slett det gamle stempelet
2. Lagre dette som PDF (CTRL+SHIFT+E export as)
3. Åpne [stempletBakgrunn.xcf](/home/fredrik/workspace/ønskeliste/media/postStempelOgBakgrunn/stempletBakgrunn.xcf) og bytt ut poststempel-PDFen ved å dra filen inn i GIMP
4. Importer PDFen som et bilde med oppløsning på ca 50 og velg fra menyen på toppen `Image - Color to alpha`, for å bli kvitt det hvite på bakgrunnen. Slett det stemplede frimerket
5. Lagre som `bakgrunn.png` og erstatt det som ligger i [pictures/](pictures)-mappen

Poststempelet skal være ca 25 mm i diameter, poststempelet kan være ca det samme og så skal de bølgede linjene være litt lavere enn diameteren på stempelet. Plasser gjerne stempelet over frimerket og forskøvet litt sidelengs.

## Lage andre poststempel

For å lage nye poststempel gjør som [hvordanLagePostStempel.md](./media/postStempelOgBakgrunn/hvordanLagePostStempel.md) sier

## English

An 2016 version is available in english in the [english-folder](english)

---

**notat**

Hvis man får denne feilen

```bash
convert-im6.q16: attempt to perform an operation not allowed by the security policy `PDF' @ error/constitute.c/IsCoderAuthorized/408
```

Pga bug i ghostscript er PDF blitt skrudd av i imagemagic (som convert bruker for å lage png og jpg av pdf). Kommenter vekk linjen som omhandler pdf i `/etc/ImageMagick-6/policy.xml`
